var zonas, regiones, comunas, segmentos, categorias, subcategorias, convenios, retorno;
var query, consulta;

google.load('visualization', '1.0', {'packages':['table', 'controls', 'corechart'], 'language': 'es'});
google.setOnLoadCallback(cargar);
consulta = "";
consulta = "SELECT A, B, C, D, F, H, I, J, K, L, R ORDER BY A ASC";

function cargar () {

	var url = "http://spreadsheets.google.com/tq?tq="+encodeURIComponent(consulta)+"&key=1lvpkvFUtjK9Xgu37HxZYWzTItw1mRx0obx8SHVf_BjE";
	//console.log(consulta);
	//console.log(url);
	query = new google.visualization.Query(url);

	query.send( handleQueryResponse );

};

function handleQueryResponse ( response ) {

	if ( response.isError() ) {
		console.log("Error in query: " + response.getMessage() + " " + response.getDetailedMessage());
		return;
	};

	var data = response.getDataTable();
	drawTable ( data );

};

function drawTable ( data ) {

	var options = {
		'width':1050, 
		'height':500,
		'page': 'enable',
		'pageSize': 50,
		'allowHtml': true,
		'cssClassNames': {
			headerCell: 'cabecera',
			tableCell: 'cuerpo',
		},
	};

	// antes era data.Lf.length
	if ( data.getNumberOfRows() === 0 ) {
		$( "#miTabla" ).empty();
		$( "#miTabla" ).append(
			'<div class="alert alert-warning" align="center" role="alert" style="font-size:11px;">' +
			'<strong>Lo sentimos.</strong>' +
			' No se encontraron coincidencias para esta búsqueda.</div>');
	} else {
		new google.visualization.Table(document.getElementById('miTabla')).draw(data, options);
	};
};

$( document ).on( "ready", function () {

	$.ajax({
		type: "get",
		url: "https://script.google.com/macros/s/AKfycbzr0bSt3_cXBJjSK6QB5WyWkoDd8EzrOJaAcZ-3C9wDrPguTCI/exec",
		success: function ( data ) {
			zonas = data.zonas;
			regiones = data.regiones;
			comunas = data.comunas;
			segmentos = data.segmentos;
			categorias = data.categorias;
			subcategorias = data.tipoBeneficio;
			convenios = data.convenios;

			llenarComboZona( zonas );
			llenarComboSegmento( segmentos );
			llenarComboCategoria( categorias );
			llenarComboSubcategoria( subcategorias );
			llenarComboConvenio( convenios );

			habilitarCombos ();

			$("#cargar").click();
		},
		error: function ( jqXHR, textStatus, errorThrown ) {
			alert( errorThrown );
		}
	});

});

function habilitarCombos () {
	$( "#institucionTxt" ).attr( "disabled", false );
	$( "#ComboZona" ).attr( "disabled", false );
	$( "#ComboRegion" ).attr( "disabled", false );
	$( "#ComboComuna" ).attr( "disabled", false );
	$( "#ComboSegmento" ).attr( "disabled", false );
	$( "#ComboCategoria" ).attr( "disabled", false );
	$( "#ComboSubCategoria" ).attr( "disabled", false );
	$( "#ComboConvenio" ).attr( "disabled", false );
	$( "#cargar" ).attr( "disabled", false );
	$( "#limpiarBusqueda" ).attr( "disabled", false );
};

function llenarComboZona ( zonas ) {

	var comboZona = $("#ComboZona");

	limpiarComboZona();
	limpiarComboRegion();
	limpiarComboComuna();

	for ( var i = 0; i < zonas.length; i++ ) {
		comboZona.append('<option value="' + zonas[i].idZona + '">' + zonas[i].zona + '</option>');
	};

};

function llenarComboRegion () {

	var comboZona = $("#ComboZona").val();
	var comboRegion = $("#ComboRegion");

	limpiarComboRegion();
	limpiarComboComuna();
	
	for ( var i = 0; i < regiones.length; i++ ) {
		if ( regiones[i].idZona == comboZona ) {
			comboRegion.append('<option value="' + regiones[i].idRegion + '">' + regiones[i].region + '</option>');
		};
	};

};

function llenarComboComuna () {

	var comboRegion = $("#ComboRegion").val();
	var comboComuna = $("#ComboComuna");

	limpiarComboComuna();

	for ( var i = 0; i < comunas.length; i++ ) {
		if ( comunas[i].idRegion == comboRegion ) {
			comboComuna.append('<option value="' + comunas[i].idComuna + '">' + comunas[i].comuna + '</option>');
		};
	};

};

function llenarComboConvenio ( convenios ) {

	var ComboConvenio = $("#ComboConvenio");

	limpiarComboConvenio();

	for ( var i = 0; i < convenios.length; i++ ) {
		ComboConvenio.append('<option value="' + i + '">' + convenios[i] + '</option>');
	};

};

function llenarComboSubcategoria ( subcategorias ) {

	var ComboSubCategoria = $("#ComboSubCategoria");

	limpiarComboSubCategoria();

	for ( var i = 0; i < subcategorias.length; i++ ) {
		ComboSubCategoria.append('<option value="' + i + '">' + subcategorias[i] + '</option>');
	};

};

function llenarComboCategoria ( categorias ) {

	var ComboCategoria = $("#ComboCategoria");

	limpiarComboCategoria();

	for ( var i = 0; i < categorias.length; i++ ) {
		ComboCategoria.append('<option value="' + i + '">' + categorias[i] + '</option>');
	};

};

function llenarComboSegmento ( segmentos ) {

	var ComboSegmento = $("#ComboSegmento");

	limpiarComboSegmento();

	for ( var i = 0; i < segmentos.length; i++ ) {
		ComboSegmento.append('<option value="' + i + '">' + segmentos[i] + '</option>');
	};
};

function limpiarComboConvenio () {
	var combo = $("#ComboConvenio");
	combo.empty();
	combo.append('<option value="">Producto o Servicio...</option>');
};

function limpiarComboSubCategoria () {
	var combo = $("#ComboSubCategoria");
	combo.empty();
	combo.append('<option value="">Tipo de beneficio...</option>');
};

function limpiarComboCategoria () {
	var combo = $("#ComboCategoria");
	combo.empty();
	combo.append('<option value="">Categoría...</option>');
};

function limpiarComboSegmento () {
	var combo = $("#ComboSegmento");
	combo.empty();
	combo.append('<option value="">Segmento...</option>');
};

function limpiarComboZona () {
	var combo = $( "#ComboZona" );
	combo.empty();
	combo.append('<option value="">Zona...</option>');
};

function limpiarComboRegion () {
	var combo = $( "#ComboRegion" );
	combo.empty();
	combo.append('<option value="">Región...</option>');
};

function limpiarComboComuna () {
	var combo = $( "#ComboComuna" );
	combo.empty();
	combo.append('<option value="">Comuna...</option>');
};

$( "#limpiarBusqueda" ).on( 'click', function () {
	$("#institucionTxt").val('');
	llenarComboZona( zonas );
	llenarComboSegmento( segmentos );
	llenarComboCategoria( categorias );
	llenarComboSubcategoria( subcategorias );
	llenarComboConvenio( convenios );

	$("#cargar").click();
});

$( "#cargar" ).on( 'click', function () {

	$( "#cargandoGif" ).show();
	$( "#miTabla" ).empty();
	$( "#miTabla" ).append(
		'<div class="well well-sm" align="center"><strong>Cargando...</strong></div>');

	var textoComercio = $( "#institucionTxt" ).val();

	var comboZona = $( "#ComboZona" );
	if ( comboZona.val() ) {
		var textoZona = comboZona.find("option:selected").text();
	} else {
		var textoZona = "";
	}
	var comboRegion = $("#ComboRegion");
	if ( comboRegion.val() ) {
		var textoRegion = comboRegion.find("option:selected").text();
	} else {
		var textoRegion = "";
	}
	var comboComuna = $("#ComboComuna");
	if ( comboComuna.val() ) {
		var textoComuna = comboComuna.find("option:selected").text();
	} else {
		var textoComuna = "";
	}
	var comboSegmento = $("#ComboSegmento");
	if ( comboSegmento.val() ) {
		var textoSegmento = comboSegmento.find("option:selected").text();
	} else {
		var textoSegmento = "";
	}
	var comboCategoria = $("#ComboCategoria");
	if ( comboCategoria.val() ) {
		var textoCategoria = comboCategoria.find("option:selected").text();
	} else {
		var textoCategoria = "";
	}
	
	var comboSubCategoria = $("#ComboSubCategoria");
	if (comboSubCategoria.val() ) {
		var textoSubCategoria = comboSubCategoria.find("option:selected").text();
	} else {
		var textoSubCategoria = "";
	}
	var comboConvenio = $("#ComboConvenio");
	if ( comboConvenio.val() ) {
		var textoConvenio = comboConvenio.find("option:selected").text();
	} else {
		var textoConvenio = "";
	}
	consulta = "SELECT A, B, C, D, F, H, I, J, K, L, R ";
	
	consulta += " WHERE lower(A) LIKE lower('%"+textoComercio+"%')";

	if (textoZona) {
		consulta += " AND C CONTAINS '"+textoZona+"'";
	}
	if (textoRegion){
		consulta += ' AND D CONTAINS "'+textoRegion+'"';
	}
	if (textoComuna){
		consulta += " AND F CONTAINS '"+textoComuna+"'";
	}
	if (textoSegmento){
		consulta += " AND G CONTAINS '"+textoSegmento+"'";
	}
	if (textoCategoria){
		consulta += " AND H CONTAINS '"+textoCategoria+"'";
	}
	if (textoSubCategoria){
		consulta += " AND I CONTAINS '"+textoSubCategoria+"'";
	}
	if (textoConvenio){
		consulta += " AND J CONTAINS '"+textoConvenio+"'";
	}
	consulta += " ORDER BY A";
	query.abort();
	cargar();
	$("#cargandoGif").hide();
});